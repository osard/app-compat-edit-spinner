package com.osard.editspinner;

import android.content.Context;
import android.content.res.ColorStateList;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * SimpleAdapter
 */
public class SimpleAdapter extends BaseEditSpinnerAdapter {
    private Context mContext;
    private List<String> mSpinnerData;
    private List<String> mCacheData;
    private int[] indexs;

    //匹配文本的颜色
    private String matchTextColor;
    //匹配文本时是否忽略字母大小写
    private boolean matchIgnoreCase;
    //待选项的文本颜色
    private ColorStateList spinnerItemTextColor;
    //待选项的文本大小，单位：SP
    private float spinnerItemTextSize;

    public SimpleAdapter(Context context, List<String> spinnerData) {
        this.mContext = context;
        this.mSpinnerData = spinnerData;
        mCacheData = new ArrayList<>(spinnerData);
        indexs = new int[mSpinnerData.size()];
    }

    @Override
    public String getItemString(int position) {
        return mSpinnerData.get(indexs[position]);
    }

    @Override
    public int getCount() {
        return mCacheData == null ? 0 : mCacheData.size();
    }

    @Override
    public String getItem(int position) {
        return mCacheData == null ? "" : mCacheData.get(position) == null ? "" : mCacheData.get(position);
    }

    /**
     * 获取富文本表示的项
     */
    public Spanned getHtmlItem(int position) {
        return Html.fromHtml(getItem(position));
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView;
        if (convertView == null) {
            textView = (TextView) LayoutInflater.from(mContext).inflate(R.layout.adapter_edit_spinner_item, null);
        } else {
            textView = (TextView) convertView;
        }
        textView.setText(getHtmlItem(position));
        if (null != spinnerItemTextColor) {
            textView.setTextColor(spinnerItemTextColor);
        }
        if (0 < spinnerItemTextSize) {
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, spinnerItemTextSize);
        }
        return textView;
    }

    /**
     * 匹配文本的颜色
     *
     * @param matchTextColor HTML表示的颜色值RGB，如：#000000
     *                       清空则不显示颜色
     */
    @Override
    public void setMatchTextColor(String matchTextColor) {
        this.matchTextColor = matchTextColor;
    }

    /**
     * 匹配时是否忽略字母大小写
     *
     * @param matchIgnoreCase 候选文本的颜色，默认黑色
     */
    @Override
    public void setMatchIgnoreCase(boolean matchIgnoreCase) {
        this.matchIgnoreCase = matchIgnoreCase;
    }

    /**
     * 待选项的文本颜色
     *
     * @param spinnerItemTextColor 候选文本的颜色，可能为null，为null时使用全局配置
     */
    public void setSpinnerItemTextColor(ColorStateList spinnerItemTextColor) {
        this.spinnerItemTextColor = spinnerItemTextColor;
    }

    public void setSpinnerItemTextSize(float spinnerItemTextSize) {
        this.spinnerItemTextSize = spinnerItemTextSize;
    }

    @Override
    public boolean onFilter(String keyword) {
        mCacheData.clear();

        if (TextUtils.isEmpty(keyword)) {
            mCacheData.addAll(mSpinnerData);
            for (int i = 0; i < indexs.length; i++) {
                indexs[i] = i;
            }
        } else {
            for (int i = 0; i < mSpinnerData.size(); i++) {
                if (matchIgnoreCase) {
                    if (!mSpinnerData.get(i).toUpperCase().contains(keyword.toUpperCase())) {
                        continue;
                    }
                } else {
                    if (!mSpinnerData.get(i).contains(keyword)) {
                        continue;
                    }
                }

                indexs[mCacheData.size()] = i;
                if (TextUtils.isEmpty(matchTextColor)) {
                    mCacheData.add(mSpinnerData.get(i));
                } else {
                    if (matchIgnoreCase) {
                        mCacheData.add(mSpinnerData.get(i));
                    } else {
                        mCacheData.add(mSpinnerData.get(i).replaceFirst(keyword, "<font color=\"" + matchTextColor + "\">" + keyword + "</font>"));
                    }
                }
            }

            //修复未匹配到数据时的扔短暂显示原搜索数据的bug
            if (mCacheData.size() <= 0) {
                notifyDataSetInvalidated();
            }
        }
        notifyDataSetChanged();
        return mCacheData.size() <= 0;
    }
}
