package com.osard.editspinner;

import android.content.res.ColorStateList;
import android.widget.BaseAdapter;

/**
 * BaseEditSpinnerAdapter
 */
public abstract class BaseEditSpinnerAdapter extends BaseAdapter {

    /**
     * editText输入监听
     */
    public abstract boolean onFilter(String keyword);

    /**
     * 获取需要填入editText的字符串
     *
     * @param position
     * @return
     */
    public abstract String getItemString(int position);

    /**
     * 匹配文本的颜色
     *
     * @param matchTextColor HTML表示的颜色值，如：#000000
     *                       清空则不显示颜色
     */
    public abstract void setMatchTextColor(String matchTextColor);

    /**
     * 匹配时是否忽略字母大小写
     *
     * @param matchIgnoreCase 候选文本的颜色，默认黑色
     */
    public abstract void setMatchIgnoreCase(boolean matchIgnoreCase);

    /**
     * 待选项的文本颜色
     *
     * @param spinnerItemTextColor 候选文本的颜色，可能为null，为null时使用全局配置
     */
    public abstract void setSpinnerItemTextColor(ColorStateList spinnerItemTextColor);

    /**
     * 待选项的文本大小，<= 0：使用全局设置
     */
    public abstract void setSpinnerItemTextSize(float spinnerItemTextSize);
}
