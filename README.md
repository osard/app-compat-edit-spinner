[![License](https://img.shields.io/badge/License%20-Apache%202-337ab7.svg)](https://www.apache.org/licenses/LICENSE-2.0)
[![API](https://img.shields.io/badge/API-21%2B-brightgreen.svg?style=flat)](https://android-arsenal.com/api?level=21)
[![](https://jitpack.io/v/com.gitee.osard/app-compat-edit-spinner.svg)](https://jitpack.io/#com.gitee.osard/app-compat-edit-spinner)


### 一、介绍
带下拉选的输入框，适用于安卓5.0，使用AndroidX

### 二、工程引入工具包准备
**com.android.tools.build:gradle:4.2.2及以下版本，在工程的 build.gradle 文件添加**

```text
allprojects {
    repositories {
        google()
        mavenCentral()

        //jitpack 仓库
        maven { url 'https://jitpack.io' }
    }
}
```

**com.android.tools.build:gradle:7.0.0及以上版本，在工程的 settings.gradle 文件添加**

```text
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()

        //jitpack 仓库
        maven {
            url 'https://jitpack.io'
        }
    }
}
```

**APP的build.gradle文件添加**
```text
dependencies {
    ...
    implementation 'com.gitee.osard:app-compat-edit-spinner:1.0.0'
    implementation 'androidx.appcompat:appcompat:1.3.0'
}
```

### 三、AppCompatEditSpinner属性

1. **Xml属性**

**所有属性均存在set方法，AppCompatEditSpinner的SimpleAdapter布局的相关属性仅适用于SimpleAdapter，若重写，则自行处理**

```xml
    <declare-styleable name="AppCompatEditSpinner">
        <!--AppCompatEditSpinner内EditText的相关属性-->
        <!--提示文本-->
        <attr name="hint" format="reference|string" />
        <!--提示文本的颜色-->
        <attr name="hintTextColor" format="reference|color" />
        <!--背景-->
        <attr name="editBackground" format="reference|color" />
        <!--字体大小，单位sp，dp自动转为sp、px不支持-->
        <attr name="editTextSize" format="reference|dimension" />
        <!--字体颜色-->
        <attr name="editTextColor" format="reference|color" />
        <!--最大输入行数-->
        <attr name="editMaxLines" format="integer" />
        <!--最大输入字符数-->
        <attr name="editMaxLength" format="integer" />
        <!--输入字符限制-->
        <attr name="editDigits" format="reference|string" />
        <!--输入字符类型-->
        <attr name="editInputType">
            <flag name="none" value="0x00000000" />
            <flag name="text" value="0x00000001" />
            <flag name="textCapCharacters" value="0x00001001" />
            <flag name="textCapWords" value="0x00002001" />
            <flag name="textCapSentences" value="0x00004001" />
            <flag name="textAutoCorrect" value="0x00008001" />
            <flag name="textAutoComplete" value="0x00010001" />
            <flag name="textMultiLine" value="0x00020001" />
            <flag name="textImeMultiLine" value="0x00040001" />
            <flag name="textNoSuggestions" value="0x00080001" />
            <flag name="textUri" value="0x00000011" />
            <flag name="textEmailAddress" value="0x00000021" />
            <flag name="textEmailSubject" value="0x00000031" />
            <flag name="textShortMessage" value="0x00000041" />
            <flag name="textLongMessage" value="0x00000051" />
            <flag name="textPersonName" value="0x00000061" />
            <flag name="textPostalAddress" value="0x00000071" />
            <flag name="textPassword" value="0x00000081" />
            <flag name="textVisiblePassword" value="0x00000091" />
            <flag name="textWebEditText" value="0x000000a1" />
            <flag name="textFilter" value="0x000000b1" />
            <flag name="textPhonetic" value="0x000000c1" />
            <flag name="textWebEmailAddress" value="0x000000d1" />
            <flag name="textWebPassword" value="0x000000e1" />
            <flag name="number" value="0x00000002" />
            <flag name="numberSigned" value="0x00001002" />
            <flag name="numberDecimal" value="0x00002002" />
            <flag name="numberPassword" value="0x00000012" />
            <flag name="phone" value="0x00000003" />
            <flag name="datetime" value="0x00000004" />
            <flag name="date" value="0x00000014" />
            <flag name="time" value="0x00000024" />
        </attr>

        <!--AppCompatEditSpinner右侧ImageView的相关属性-->
        <!--图片-->
        <attr name="rightImage" format="reference|color" />
        <!--是否隐藏下拉选-->
        <attr name="rightImageGone" format="boolean" />
        <!--右侧图片展开下拉选时是否显示全部数据，默认：点击时显示和当前输入匹配的数据-->
        <attr name="rightImageDropShowAllItem" format="boolean" />

        <!--AppCompatEditSpinner的ListPopupWindow布局的相关属性-->
        <!--待选项的背景，drawable或color资源-->
        <attr name="spinnerBackground"  format="reference|color" />
        
        <!--AppCompatEditSpinner的SimpleAdapter布局的相关属性-->
        <!--待选项的字体大小，单位sp，dp自动转为sp、px不支持-->
        <attr name="spinnerItemTextSize" format="reference|dimension" />
        <!--待选项的颜色，默认黑色-->
        <attr name="spinnerItemTextColor" format="reference|color" />
        <!--匹配字符的颜色，默认无颜色-->
        <attr name="spinnerItemMatchTextColor" format="reference|color" />
        <!--匹配字符时是否忽略字母大小写，默认不忽略。忽略时“spinnerItemTextColor”属性无效-->
        <attr name="spinnerItemMatchIgnoreCase" format="boolean" />
    </declare-styleable>
```

2. **Java属性**

```java
public class AppCompatEditSpinner {
    /**
     * 使用SimpleAdapter加载数据
     * <p>
     * 需要在
     * {@link AppCompatEditSpinner#setMatchIgnoreCase(boolean)}、
     * {@link AppCompatEditSpinner#setMatchTextColor(String)}、
     * {@link AppCompatEditSpinner#setSpinnerItemTextColor(int)}、
     * {@link AppCompatEditSpinner#setSpinnerItemTextSize(int)}、
     * {@link AppCompatEditSpinner#setSpinnerBackground(int)}
     * 属性设置之后调用。
     */
    public void setItemData(List<String> data);
    
     /**
     * 使用继承{@link BaseEditSpinnerAdapter}的适配器加载数据
     * <p>
     * 需要在
     * {@link AppCompatEditSpinner#setMatchIgnoreCase(boolean)}、
     * {@link AppCompatEditSpinner#setMatchTextColor(String)}、
     * {@link AppCompatEditSpinner#setSpinnerItemTextColor(int)}、
     * {@link AppCompatEditSpinner#setSpinnerItemTextSize(int)}、
     * {@link AppCompatEditSpinner#setSpinnerBackground(int)}
     * 属性设置之后调用
     */
    public void setAdapter(BaseEditSpinnerAdapter adapter);
    
     /**
     * 设置文本
     */
    public void setText(@StringRes int text);

    /**
     * 设置文本
     */
    public void setText(String text);

    /**
     * 设置Hint文本
     */
    public void setHint(@StringRes int hint);

    /**
     * 设置Hint文本
     */
    public void setHint(String hint);

    /**
     * 设置Hint的文本颜色
     */
    public void setHintTextColor(@ColorInt int color);

    /**
     * 设置背景
     */
    public void setEditBackgroundResource(@DrawableRes int resource);

    /**
     * 设置输入文字字体大小，单位：sp
     */
    public void setEditTextSize(int size);

    /**
     * 设置输入文字颜色
     */
    public void setEditTextColor(@ColorInt int color);

    /**
     * 设置输入限制最大行数
     */
    public void setEditMaxLines(int maxLines);

    /**
     * 设置输入限制最大字符长度
     */
    public void setEditMaxLength(int maxLength);

    /**
     * 设置输入限制类型
     * <p>
     * 例如：{@link  InputType#TYPE_CLASS_TEXT}
     */
    public void setEditInputType(int inputType);

    /**
     * 设置输入限制字符
     */
    public void setEditDigits(@StringRes int digits);

    /**
     * 设置输入限制字符
     */
    public void setEditDigits(String digits);

    /**
     * 设置是否隐藏右侧下拉选图标
     */
    public void setRightImageGone(boolean rightImageGone);

    /**
     * 右侧图片展开下拉选时是否显示全部数据，默认：点击时显示和当前输入匹配的数据
     */
    public void setRightImageDropShowAllItem(boolean rightImageDropShowAllItem);

    /**
     * 匹配字符的颜色，忽略字母大小写时，此属性无效
     *
     * <p>
     * 必须在{@link AppCompatEditSpinner#setItemData(List)}
     * 或者{@link AppCompatEditSpinner#setAdapter(BaseEditSpinnerAdapter)} 调用之前进行设置
     * <p>
     * {@link AppCompatEditSpinner#setItemData(List)}
     * 或者{@link AppCompatEditSpinner#setAdapter(BaseEditSpinnerAdapter)} 调用后不可再次修改
     *
     * @param matchTextColor RGB形式，需要包含#号，例如：“#000000”
     */
    public void setMatchTextColor(String matchTextColor);

    /**
     * 匹配字符时是否忽略字母大小写，默认不忽略。
     *
     * <p>
     * 必须在{@link AppCompatEditSpinner#setItemData(List)}
     * 或者{@link AppCompatEditSpinner#setAdapter(BaseEditSpinnerAdapter)}调用之前进行设置
     * <p>
     * {@link AppCompatEditSpinner#setItemData(List)}
     * 或者{@link AppCompatEditSpinner#setAdapter(BaseEditSpinnerAdapter)} 调用后不可再次修改
     */
    public void setMatchIgnoreCase(boolean matchIgnoreCase);

    /**
     * 待选项的颜色，默认黑色
     *
     * <p>
     * 必须在{@link AppCompatEditSpinner#setItemData(List)}
     * 或者{@link AppCompatEditSpinner#setAdapter(BaseEditSpinnerAdapter)}调用之前进行设置
     * <p>
     * {@link AppCompatEditSpinner#setItemData(List)}
     * 或者{@link AppCompatEditSpinner#setAdapter(BaseEditSpinnerAdapter)} 调用后不可再次修改
     */
    public void setSpinnerItemTextColor(@ColorInt int spinnerItemTextColor);

    /**
     * 待选项的字体大小，单位sp。
     *
     * <p>
     * 必须在{@link AppCompatEditSpinner#setItemData(List)}
     * 或者{@link AppCompatEditSpinner#setAdapter(BaseEditSpinnerAdapter)}调用之前进行设置
     * <p>
     * {@link AppCompatEditSpinner#setItemData(List)}
     * 或者{@link AppCompatEditSpinner#setAdapter(BaseEditSpinnerAdapter)} 调用后不可再次修改
     */
    public void setSpinnerItemTextSize(int spinnerItemTextSize);

    /**
     * 待选项的背景
     *
     * <p>
     * 必须在{@link AppCompatEditSpinner#setItemData(List)}
     * 或者{@link AppCompatEditSpinner#setAdapter(BaseEditSpinnerAdapter)}调用之前进行设置
     * <p>
     * {@link AppCompatEditSpinner#setItemData(List)}
     * 或者{@link AppCompatEditSpinner#setAdapter(BaseEditSpinnerAdapter)} 调用后不可再次修改
     */
    public void setSpinnerBackground(Drawable spinnerBackground);

    /**
     * 待选项的背景
     *
     * <p>
     * 必须在{@link AppCompatEditSpinner#setItemData(List)}
     * 或者{@link AppCompatEditSpinner#setAdapter(BaseEditSpinnerAdapter)}调用之前进行设置
     * <p>
     * {@link AppCompatEditSpinner#setItemData(List)}
     * 或者{@link AppCompatEditSpinner#setAdapter(BaseEditSpinnerAdapter)} 调用后不可再次修改
     */
    @SuppressLint("UseCompatLoadingForDrawables")
    public void setSpinnerBackground(@DrawableRes int spinnerBackground);

    /**
     * 获取输入的文本
     */
    public String getText();

    /**
     * 设置下拉选图标
     */
    public void setRightImageDrawable(Drawable drawable);

    /**
     * 设置下拉选图标
     */
    public void setRightImageResource(@DrawableRes int res);

    /**
     * 获取输入控件EditText，可做特殊配置
     */
    public AppCompatEditText getEditText();

    /**
     * 获取右侧ImageView，可做特殊配置
     */
    public AppCompatImageView getRightImageView();
}
```

### 四、可覆盖参数

**colors.xml**
```xml
    <!--右侧下拉选图标颜色-->
    <color name="ic_expand_more_black_color">#000000</color>
```

**styles.xml**
```xml
    <!--EditSpinner，内部EditView的风格控制，重写风格后可变更属性进行调整（统一风格）-->
    <style name="edit_spinner_edit_sty">
        <item name="android:background">@drawable/bg_view_frame</item>
        <item name="android:textColor">@android:color/black</item>
        <item name="android:textSize">14sp</item>
        <item name="android:padding">8dp</item>
    </style>

    <!--EditSpinner，内部ImageView的风格控制（右侧图片），重写风格后可变更属性进行调整（统一风格）-->
    <style name="edit_spinner_image_sty">
        <item name="android:padding">8dp</item>
        <item name="android:src">@drawable/ic_expand_more_black</item>
    </style>

    <!--EditSpinner的SimpleAdapter布局控制，内部TextView的风格控制，重写风格后可变更属性进行调整（统一风格）-->
    <style name="simple_adapter_text_view_item_sty">
        <item name="android:padding">5dp</item>
        <item name="android:textColor">#000000</item>
    </style>
```



License
-------

    Copyright 2022 mjsoftking

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.



