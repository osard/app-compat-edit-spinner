package com.osard.app.editspinner;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.osard.app.editspinner.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        binding.editSpinner3.setSpinnerBackground(R.drawable.bg_view_frame_app);

        initData();
    }

    private void initData() {
        List<String> list = new ArrayList<>();
        list.add("Hello World");
        list.add("Test");
        list.add("测试");
        list.add("汉字");
        list.add("one");
        list.add("two");
        list.add("123456789");
        list.add("1232");
        list.add("1234562121777");
        list.add("123213211");
        list.add("1233331231333");
        list.add("12222221321222");
        list.add("122344414");
        list.add("1232442133");
        list.add("122222132222");
        binding.editSpinner1.setItemData(list);
        binding.editSpinner2.setItemData(list);
        binding.editSpinner3.setItemData(list);
        binding.editSpinner4.setItemData(list);
    }
}
